export default function (state = null, action = {}) {
    switch(action.type) {
        case 'ACTIVATE_MENU': 
            return action.payload
        default: 
            return state
    }
}