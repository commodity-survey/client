const initialState = localStorage.getItem('session') ? JSON.parse(localStorage.getItem('session')) : {}
export default function (state = initialState, action = {}) {
    let nextState = {}
    switch (action.type) {
        case 'AUTHENTICATE':
            nextState = { authenticate: true, token: action.payload, userinfo: {} }
            localStorage.setItem('session', JSON.stringify(nextState)) 
            return nextState
        case 'AUTHORIZE':
            nextState = {...state, authorize: true, userinfo: action.payload }
            localStorage.setItem('session', JSON.stringify(nextState)) 
            return nextState
        case 'LOGOUT':
            nextState = { authenticate: false, authorize: false, token: null, userinfo: {} }
            localStorage.setItem('session', JSON.stringify(nextState)) 
            return nextState
        default: 
            return state
    }
}