import { combineReducers } from 'redux'

import userinfo from './reducers/userinfo'
import menu from './reducers/menu'
import loader from './reducers/loader'
import session from './reducers/session'

export default combineReducers({
    userinfo,
    menu,
    loader,
    session,
})