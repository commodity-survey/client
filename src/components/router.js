import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import { connect } from 'react-redux'

import qs from 'querystring'
import env from '../config/env'

import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client'
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from '@apollo/client/link/context';

import Middleware from './middleware/middleware'

import PrivateRoute from './middleware/private-router'
import Dashboard from './pages/dashboard'

import Category from './pages/category'
import Unit from './pages/unit'
import Commodity from './pages/commodity'
import Survey from './pages/survey'
import SurveyApproval from './pages/survey-approval'

import Account from './pages/account'
import AccountDetail from './pages/account.detail'
import LandingPage from './pages/landing-page'
import Login from './pages/login'

function Router ({ session }) {
    // Setup Apollo Client
    const __baseurl = `http://${env.hostname}/${env.api}/graphql`

    const authLink = setContext((_, { headers }) => {
        const authorization = qs.stringify({token: session.token})
        return { headers: { ...headers, authorization } }
    })

    const client = new ApolloClient({ 
        link: authLink.concat(createUploadLink({ uri: __baseurl })), 
        cache: new InMemoryCache(), 
        errorPolicy: 'all' 
    })

    return (
        <ApolloProvider client={client}>
            <Middleware>
                <BrowserRouter>
                    <Route exact path='/' component={ LandingPage } />
                    <Route path='/login' component={ Login } />
                    <PrivateRoute path='/admin' component={ Dashboard } theme = 'admin'>
                        <Route path='/admin/dashboard' component={ Dashboard } />
                        
                        <Route path='/admin/category' component={ Category } />
                        <Route path='/admin/unit' component={ Unit } />
                        <Route path='/admin/commodity' component={ Commodity } />
                        <Route path='/admin/survey' component={ Survey } />
                        <Route path='/admin/survey-approval' component={ SurveyApproval } />

                        <Route exact path='/admin/account' component={ Account } />
                        <Route exact path='/admin/account/:username' component={ AccountDetail } activeMenu="/admin/account" />
                    </PrivateRoute>
                </BrowserRouter>
            </Middleware>
        </ApolloProvider>
    )
}

const mapStateToProps = state => ({
    session: state.session,
})

export default connect(mapStateToProps)(Router)