import React from 'react'
import { connect } from "react-redux";
import { Route, withRouter, Redirect } from 'react-router-dom'
import Admin from '../layout/admin'

export class Routes extends React.Component {
    render(props) {
        const Component = withRouter(this.props.component)
        const Theme = this.props.theme === 'admin' ? withRouter(Admin) : null
        // const state = store.getState()
        if(this.props.session.authenticate && this.props.session.authorize) {
            if(Theme) {
                return (<Route path={this.props.path} render={() => <Theme {...this.props} />} />)
            } else {
                return <Route path={this.props.path} render={() => Component}></Route>
            }
        } else {
            return <Route path={this.props.path} render={() => <Redirect to='/login' />} />
        }
    }
}

const mapStateToProps = state => ({
    session: state.session
})

export default connect(mapStateToProps)(withRouter(Routes))