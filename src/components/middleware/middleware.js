import { connect } from 'react-redux'
import { gql, useQuery } from '@apollo/client'

const Middleware = ({ children, session, authorize }) => {
    const userinfoQuery = gql`{ userinfo }`

    const { loading } = useQuery(userinfoQuery, { 
        onCompleted: (response) => {
            if(response && !session.authorize) {
                authorize(response.userinfo)
            }
        }
    })

    return !loading ? children : null
}

const mapStateToProps = state => ({
    session: state.session
})

const mapDispatchToProps = dispatch => {
    return {
        authorize: (payload) => dispatch({ type: 'AUTHORIZE', payload }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Middleware)