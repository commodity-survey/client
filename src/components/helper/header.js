import React from 'react'
import { Breadcrumbs, Link, Typography, Box, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles({
    bcrumbs: {
        fontSize: '14px',
        margin: '3px 0px !important',
    },
    title: {
        marginLeft: '5px',
        padding: '10px 5px',
        borderBottom: '2px solid lightgrey',
        '& .header-text': {
            fontWeight: 'bold'
        }
    },
})

export default ({title, subtitle, breadcrumb, children, variant}) => {
    const classes = useStyles()
    return (
        <Box classes={{root: classes.title}}>
            <Grid container spacing={3}>
                <Grid item xs={10}>
                    <Typography className="header-text" variant={variant?variant:'h6'}>{title}</Typography>
                    {subtitle ? (
                        <Typography color="textSecondary">{subtitle}</Typography>
                    ):null}
                    {breadcrumb ? (
                        <Breadcrumbs classes={{root: classes.bcrumbs}} aria-label="breadcrumb">
                            {breadcrumb.map((val, i) => {
                                return (
                                    <Link 
                                        color={val.active?'inherit':'primary'} 
                                        underline="none" 
                                        href={val.active?'#':val.url} 
                                        key={`breadcrumb${i}`}>
                                        {val.display}
                                    </Link>
                                )
                            })}
                        </Breadcrumbs>
                    ) : null}
                </Grid>
                <Grid item xs={2}>{children}</Grid>
            </Grid>
        </Box>
    )
}