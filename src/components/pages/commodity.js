import React from 'react'
import Header from '../helper/header'
import { Box, Button, TextField, Fab } from '@material-ui/core'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from '@material-ui/core'
import { EditOutlined } from '@material-ui/icons'
import { Autocomplete } from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'
import {gql, useQuery, useMutation} from '@apollo/client'
import { useSnackbar } from 'notistack'
import { Add } from '@material-ui/icons'
import { useFormik } from 'formik'
import * as Yup from 'yup'

import MaterialTableModified from '../helper/material-table'

const useStyles = makeStyles({
    root: {
        marginTop: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
})

export default function Commodity (props) {
    const classes = useStyles()
    const [openAddDialog, setOpenAddDialog] = React.useState(false)
    const [openEditDialog, setOpenEditDialog] = React.useState(false)
    const [editCommodityID, setEditCommodityID] = React.useState(null)
    const {enqueueSnackbar} = useSnackbar()
    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Category', field: 'category_name' },
        { title: 'Commodity Name', field: 'commodity_name' },
        { title: 'Unit of Commodity', field: 'unit_name' },
    ])
    
    const getCommodityQuery = gql`{
        commodity { get }
        category { get }
        unit { get }
    }`
    const {loading, data, refetch} = useQuery(getCommodityQuery)
    const editableData = !loading ? data.commodity.get.map(o => ({ ...o })) : []

    const AddCommodityForm = () => {
        const AddCommodityMutation = gql`
            mutation addCommodity($commodity_name: String!, $category_id: String!, $unit_id: String!) {
                commodity {
                    create(commodity_name: $commodity_name, category_id: $category_id, unit_id: $unit_id)
                }
            }
        `
        const [AddCommodityRequest] = useMutation(AddCommodityMutation)

        const formik = useFormik({
            initialValues: {
                commodity_name: '',
                category: '',
                unit: '',
            },
            validationSchema: Yup.object({
                commodity_name: Yup.string().required('Commodity Name is required.'),
                category: Yup.string().required('Category is required.'),
                unit: Yup.string().required('Unit of Commodity is required.'),
            }),
            onSubmit: values => {
                let modifiedValues = {
                    commodity_name: values.commodity_name,
                    category_id: values.category.id,
                    unit_id: values.unit.id,
                }

                AddCommodityRequest({variables: modifiedValues}).then((response) => {
                    setOpenAddDialog(false)
                    enqueueSnackbar('Commodity is successfully added.', {variant: 'success'})
                    refetch()
                }).catch((e) => {
                    setOpenAddDialog(false)
                    console.log(e)
                })
            }
        })

        return (
            <Dialog open={openAddDialog} onClose={() => setOpenAddDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle id="form-dialog-title">
                        Add Commodity Data
                        <DialogContentText>
                            To add commodity data, please fill all of form inputs here.
                        </DialogContentText>
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            helperText={formik.errors.commodity_name}
                            margin="dense"
                            onChange={formik.handleChange}
                            id="commodity_name" label="Commodity Name" type="text" fullWidth
                            value={formik.values.commodity_name}/>
                        <Autocomplete
                            id="category-autocomplete"
                            onChange={(e, value) => formik.setFieldValue('category', value)}
                            options={!loading ? data.category.get : []}
                            getOptionLabel={(option) => option.category_name }
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formik.errors.category} 
                                    label="Choose Category" 
                                    margin="dense" 
                                />
                            }/>
                        <Autocomplete
                            id="unit-autocomplete"
                            onChange={(e, value) => formik.setFieldValue('unit', value)}
                            options={!loading ? data.unit.get : []}
                            getOptionLabel={(option) => option.unit_name }
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formik.errors.unit} 
                                    label="Choose Unit of Commodity" 
                                    margin="dense" 
                                />
                            }/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenAddDialog(false)} variant="contained" size="small" color="default">
                            Cancel
                        </Button>
                        <Button type="submit" variant="contained" size="small" color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    const EditCommodityForm = () => {
        const editedValue = data.commodity.get.filter((el) => el.id === editCommodityID)[0]
        const editedCategory = data.category.get.filter((el) => el.id === editedValue.category_id)[0]
        const editedUnit = data.unit.get.filter((el) => el.id === editedValue.unit_id)[0]

        const EditCommodityMutation = gql`
            mutation editCommodity($id: String!, $commodity_name: String!, $category_id: String!, $unit_id: String!) {
                commodity {
                    update(id: $id, commodity_name: $commodity_name, category_id: $category_id, unit_id: $unit_id)
                }
            }
        `
        const [EditCommodityRequest] = useMutation(EditCommodityMutation)

        const formik = useFormik({
            initialValues: {
                commodity_name: editedValue.commodity_name,
                category: editedCategory,
                unit: editedUnit,
            },
            validationSchema: Yup.object({
                commodity_name: Yup.string().required('Commodity Name is required.'),
                category: Yup.string().required('Category is required.'),
                unit: Yup.string().required('Unit of Commodity is required.'),
            }),
            onSubmit: values => {
                let modifiedValues = {
                    id: editedValue.id,
                    commodity_name: values.commodity_name,
                    category_id: values.category.id,
                    unit_id: values.unit.id,
                }

                EditCommodityRequest({variables: modifiedValues}).then((response) => {
                    setOpenEditDialog(false)
                    enqueueSnackbar('Commodity is successfully edited.', {variant: 'success'})
                    refetch()
                }).catch((e) => {
                    setOpenEditDialog(false)
                    console.log(e)
                })
            }
        })

        return (
            <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle id="form-dialog-title">
                        Edit Commodity Data
                        <DialogContentText>
                            To edit commodity data, please fill all of form inputs here.
                        </DialogContentText>
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            helperText={formik.errors.commodity_name}
                            margin="dense"
                            onChange={formik.handleChange}
                            id="commodity_name" label="Commodity Name" type="text" fullWidth
                            value={formik.values.commodity_name}/>
                        <Autocomplete
                            id="category-autocomplete"
                            onChange={(e, value) => formik.setFieldValue('category', value)}
                            options={!loading ? data.category.get : []}
                            getOptionLabel={(option) => option.category_name }
                            value={formik.values.category}
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formik.errors.category} 
                                    label="Choose Category" 
                                    margin="dense" 
                                />
                            }/>
                        <Autocomplete
                            id="unit-autocomplete"
                            onChange={(e, value) => formik.setFieldValue('unit', value)}
                            options={!loading ? data.unit.get : []}
                            getOptionLabel={(option) => option.unit_name }
                            value={formik.values.unit}
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formik.errors.unit} 
                                    label="Choose Unit of Commodity" 
                                    margin="dense" 
                                />
                            }/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenEditDialog(false)} variant="contained" size="small" color="default">
                            Cancel
                        </Button>
                        <Button type="submit" variant="contained" size="small" color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    const DeleteCommodityMutation = gql`
        mutation deleteCommodity($id: String!) {
            commodity {
                delete(id: $id)
            }
        }
    `
    const [DeleteCommodityRequest] = useMutation(DeleteCommodityMutation)

    return (
        <Box component="div">
            <Header 
                title="Commodity"
                breadcrumb={[
                    {display: 'Master Data', active: true},
                    {display: 'Commodity', active: true}
                ]}>
                    <Box component="div" textAlign="right">
                        <Fab color="primary" size="small" onClick={() => setOpenAddDialog(true)}>
                            <Add /> 
                        </Fab>
                    </Box>
            </Header>
            <Box component="div" className={classes.root}>
                <MaterialTableModified
                    title=""
                    columns={columns}
                    data={editableData}
                    options={{actionsColumnIndex: 5}}
                    actions= {[
                        {
                            icon: EditOutlined,
                            tooltip: 'Edit Commodity',
                            onClick: (event, rowData) => {
                                setEditCommodityID(rowData.id)
                                setOpenEditDialog(true)
                            }
                        },
                    ]}
                    editable={{
                        onRowDelete: (oldData) => {
                            return DeleteCommodityRequest({variables: {id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Commodity is successfully deleted.', {variant: 'success'})
                                refetch()
                            }).catch((e) => {
                                console.log(e)
                            })
                        }
                    }}
                />
            </Box>
            <AddCommodityForm />
            {openEditDialog ? <EditCommodityForm /> : null}
        </Box>
    )
}