import React from 'react'
import Header from '../helper/header'
import { Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import MaterialTable from '../helper/material-table';
import {gql, useQuery, useMutation} from '@apollo/client'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles({
    root: {
        marginTop: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
})

export default function Category (props) {
    const classes = useStyles()
    const {enqueueSnackbar} = useSnackbar()
    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Category Name', field: 'category_name' },
    ])
    
    const getCategoryQuery = gql`{category{ get }}`
    const {loading, data, refetch} = useQuery(getCategoryQuery)
    const editableData = !loading ? data.category.get.map(o => ({ ...o })) : []

    const addCategoryMutation = gql`
        mutation addCategory($category_name: String!) {
            category {
                create(category_name: $category_name)
            }
        }
    `
    const updateCategoryMutation = gql`
        mutation updateCategory($id: String!, $category_name: String!) {
            category {
                update(id: $id, category_name: $category_name)
            }
        }
    `
    const deleteCategoryMutation = gql`
        mutation deleteCategory($id: String!) {
            category {
                delete(id: $id)
            }
        }
    `
    const [addCategoryRequest] = useMutation(addCategoryMutation)
    const [updateCategoryRequest] = useMutation(updateCategoryMutation)
    const [deleteCategoryRequest] = useMutation(deleteCategoryMutation)

    return (
        <Box component="div">
            <Header 
                title="Category"
                breadcrumb={[
                    {display: 'Master Data', active: true},
                    {display: 'Category', active: true}
                ]}/>
            <Box component="div" className={classes.root}>
                <MaterialTable
                    title=""
                    columns={columns}
                    data={editableData}
                    options={{ actionsColumnIndex: 2 }}
                    editable={{
                        onRowAdd: async (newData) => {
                            return addCategoryRequest({variables: newData}).then((response) => {
                                enqueueSnackbar('Category is successfully added.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowUpdate: async (newData, oldData) => {
                            return updateCategoryRequest({variables: {...newData, id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Category is successfully updated.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowDelete: async (oldData) => {
                            return deleteCategoryRequest({variables: {id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Category is successfully deleted.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                    }}
                />
            </Box>
        </Box>
    )
}