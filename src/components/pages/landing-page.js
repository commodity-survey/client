import React from 'react'
import { Typography, Grid, Paper, Box, Button, CssBaseline } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import CommodityImage from '../../images/commodity.png'

import DateFnsUtils from '@date-io/date-fns'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import NumberFormat from 'react-number-format'

import MaterialTableModified from '../helper/material-table'
import { gql, useQuery } from '@apollo/client'
import { useFormik } from 'formik'

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
        background: 'linear-gradient(30deg, rgba(0,245,255,1) 10%, rgba(136,128,243,1) 57%, rgba(241,37,233,1) 90%)',
        position: 'absolute',
        width: '100%'
    },
    brand: {
        padding: '15px 30px',
        '& .brand-text': {
            fontSize: '2rem',
            fontWeight: 'bold',
            color: '#fff'
        }
    },
    content: {
        margin: '20px',
        '& .container-subtitle': {
            width: '100%',
            textAlign: 'center',
            '& .subtitle-text': {
                color: '#fff',
                fontSize: '1.25rem',
                fontWeight: 'bold',
                margin: '10px 0px'
            }
        },
        '& .container-image': {
            width: '100%',
            textAlign: 'center',
            marginTop: '10px',
            marginLeft: '10px',
            '& img': {
                width: '568px',
                maxWidth: '95%',
                margin: '10px auto'
            }
        },
        '& .container-data': {
            marginTop: '10%',
            padding: '8px 10px',
            boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
            '& .container-filter': {
                padding: '0px 10px',
                '&-text': {
                    fontSize: '1.25rem',
                    padding: '20px 0px 0px 0px',
                    fontWeight: 'bold'
                }
            }
        }
    }
}))

export default function LandingPage(props) {
    const classes = useStyles()
    const dateFns = new DateFnsUtils()

    const formFilter = useFormik({
        initialValues: {
            filter_date_min: dateFns.format(new Date(), 'yyyy-MM-dd'),
            filter_date_max: dateFns.format(new Date(), 'yyyy-MM-dd'),
        },
    })

    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Survey Date', render: (row) => dateFns.format(new Date(row.survey_date), 'dd MMM yyyy') },
        { title: 'Category', field: 'category_name' },
        { title: 'Commodity Name', field: 'commodity_name' },
        { 
            title: 'Commodity Price', 
            render: (row) => <NumberFormat value={row.commodity_price} displayType={'text'} thousandSeparator={true} prefix={'Rp '} /> 
        },
    ])

    const getSurveyQuery = gql`query getSurvey($filter_date_min: String!, $filter_date_max: String!) {
        visitor { get(filter_date_min: $filter_date_min, filter_date_max: $filter_date_max) }
    }`
    const {loading, data} = useQuery(getSurveyQuery, { variables: {
        filter_date_max: formFilter.values.filter_date_max,
        filter_date_min: formFilter.values.filter_date_min,
    }})
    const editableData = !loading ? data.visitor.get.map(o => ({ ...o })) : []

    return (
        <Paper className={classes.root}>
            <CssBaseline />
            <Box className={classes.brand}>
                <Typography className='brand-text'>Commodity Survey</Typography>
            </Box>
            <Box className={classes.content}>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={6}>
                        <Box className="container-image">
                            <img src={CommodityImage} alt="Commodity Survey"/>
                        </Box>
                        <Box className="container-subtitle">
                            <Typography className="subtitle-text">Commodity Pricing, Systemic Risk & Systematic Value</Typography>
                            <Button variant="contained" color="primary" onClick={() => props.history.push('/login')}>Login as Administrator</Button>
                        </Box>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Paper className={'container-data'}>
                            <Paper className="container-filter">
                                <Grid container spacing={2}>
                                    <Grid item xs={12} md={6}>
                                        <Typography variant='h6' className="container-filter-text">Comodity Survey Price</Typography>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <Grid container spacing={3} justify="flex-end">
                                                <Grid item xs={12} md={6}>
                                                    <KeyboardDatePicker
                                                        id="filter-date-min"
                                                        variant="inline"
                                                        format="yyyy-MM-dd"
                                                        margin="normal"
                                                        label="Filter by Date Min"
                                                        maxDate={new Date()}
                                                        helperText={formFilter.errors.filter_date_min}
                                                        value={formFilter.values.filter_date_min}
                                                        onChange={(e, value) => formFilter.setFieldValue('filter_date_min', value)}
                                                        KeyboardButtonProps={{ 'aria-label': 'Survey Date Min' }}
                                                        fullWidth
                                                    />
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <KeyboardDatePicker
                                                        id="filter-date-max"
                                                        variant="inline"
                                                        format="yyyy-MM-dd"
                                                        margin="normal"
                                                        label="Filter by Date Max"
                                                        maxDate={new Date()}
                                                        helperText={formFilter.errors.filter_date_max}
                                                        value={formFilter.values.filter_date_max}
                                                        onChange={(e, value) => formFilter.setFieldValue('filter_date_max', value)}
                                                        KeyboardButtonProps={{ 'aria-label': 'Survey Date max' }}
                                                        fullWidth
                                                    />
                                                </Grid>
                                            </Grid>
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                </Grid>
                            </Paper>
                            <MaterialTableModified
                                title=""
                                columns={columns}
                                data={editableData}
                            />
                        </Paper>
                    </Grid>
                </Grid>
            </Box>
        </Paper>
    )
}
