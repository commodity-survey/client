import React from 'react'
import Header from '../helper/header'
import { Box, Button, TextField, Fab, Grid, Typography, Chip } from '@material-ui/core'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from '@material-ui/core'
import { EditOutlined } from '@material-ui/icons'
import { Autocomplete } from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'

import DateFnsUtils from '@date-io/date-fns'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import NumberFormat from 'react-number-format'

import { gql, useQuery, useMutation } from '@apollo/client'
import { useSnackbar } from 'notistack'
import { Add, CachedOutlined, DoneOutlined, ThumbDownAltOutlined } from '@material-ui/icons'
import { useFormik } from 'formik'
import * as Yup from 'yup'

import MaterialTableModified from '../helper/material-table'

const useStyles = makeStyles({
    root: {
        marginTop: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    },
    form: {
        margin: '10px 0px',
        padding: '8px 10px',
        boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
        '& .title': {
            fontWeight: 'bold',
            fontSize: '1rem'
        },
        '& .action-container': {
            marginTop: '10px',
            width: '100%',
            textAlign: 'right'
        }
    },
})

export default function Survey (props) {
    const classes = useStyles()
    const dateFns = new DateFnsUtils()
    const [openAddDialog, setOpenAddDialog] = React.useState(false)
    const [openEditDialog, setOpenEditDialog] = React.useState(false)
    const [editSurveyID, setEditSurveyID] = React.useState(null)
    const {enqueueSnackbar} = useSnackbar()

    const formFilter = useFormik({
        initialValues: {
            filter_date_min: dateFns.format(new Date(), 'yyyy-MM-dd'),
            filter_date_max: dateFns.format(new Date(), 'yyyy-MM-dd'),
            commodity: [],
            category: [],
        },
    })

    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Survey Date', render: (row) => dateFns.format(new Date(row.survey_date), 'dd MMM yyyy') },
        { title: 'Category', field: 'category_name' },
        { title: 'Commodity Name', field: 'commodity_name' },
        { 
            title: 'Commodity Price', 
            render: (row) => <NumberFormat value={row.commodity_price} displayType={'text'} thousandSeparator={true} prefix={'Rp '} /> 
        },
        { 
            title: 'Approval', 
            render: (row) => {
                if(row.approved === 0) {
                    return <Chip
                        icon={<CachedOutlined />}
                        label="Waiting Approval"
                        variant="outlined"
                    />
                } else if (row.approved === 1) {
                    return <Chip
                        icon={<DoneOutlined />}
                        label="Approved"
                        color="primary"
                        variant="outlined"
                    />
                } else {
                    return <Chip
                        icon={<ThumbDownAltOutlined />}
                        label="Rejected"
                        color="secondary"
                        variant="outlined"
                    />
                }
            }
        },
    ])

    const modifiedValuesFilter = {
        filter_date_min: formFilter.values.filter_date_min,
        filter_date_max: formFilter.values.filter_date_max,
        commodity: formFilter.values.commodity.map(val=>val.id),
        category: formFilter.values.category.map(val=>val.id)
    }

    const getSurveyQuery = gql`
        query getSurveyQuery($filter_date_min: String, $filter_date_max: String, $commodity: [String], $category: [String]){
            surveyor { 
                getForSurveyor(
                    filter_date_min: $filter_date_min, 
                    filter_date_max: $filter_date_max, 
                    commodity: $commodity, 
                    category: $category
                ) 
            }
            commodity { get }
            category { get }
            unit { get }
        }`
    const {loading, data, refetch} = useQuery(getSurveyQuery, {variables: modifiedValuesFilter})
    const editableData = !loading ? data.surveyor.getForSurveyor.map(o => ({ ...o })) : []

    const AddSurveyForm = () => {
        const AddSurveyMutation = gql`
            mutation addSurvey($survey_date: String!, $commodity_id: String!, $commodity_price: Int!) {
                survey {
                    create(survey_date: $survey_date, commodity_id: $commodity_id, commodity_price: $commodity_price)
                }
            }
        `
        const [AddSurveyRequest] = useMutation(AddSurveyMutation)

        const formik = useFormik({
            initialValues: {
                survey_date: dateFns.format(new Date(), 'yyyy-MM-dd'),
                commodity: '',
                commodity_price: '',
            },
            validationSchema: Yup.object({
                survey_date: Yup.string().required('Survey Date is required.'),
                commodity: Yup.string().required('Commodity is required.'),
                commodity_price: Yup.string().required('Commodity Price is required.'),
            }),
            onSubmit: values => {
                let modifiedValues = {
                    survey_date: values.survey_date,
                    commodity_id: values.commodity.id,
                    commodity_price: parseInt(values.commodity_price),
                }

                AddSurveyRequest({variables: modifiedValues}).then((response) => {
                    setOpenAddDialog(false)
                    enqueueSnackbar('Survey is successfully added.', {variant: 'success'})
                    refetch()
                }).catch((e) => {
                    setOpenAddDialog(false)
                    console.log(e)
                })
            }
        })

        return (
            <Dialog open={openAddDialog} onClose={() => setOpenAddDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle id="form-dialog-title">
                        Add Survey Commodity Data
                        <DialogContentText>
                            To add survey commodity data, please fill all of form inputs here.
                        </DialogContentText>
                    </DialogTitle>
                    <DialogContent>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                id="survey-date"
                                variant="inline"
                                format="yyyy-MM-dd"
                                margin="dense"
                                label="Survey Date"
                                maxDate={new Date()}
                                helperText={formik.errors.survey_date}
                                value={formik.values.survey_date}
                                onChange={(e, value) => formik.setFieldValue('survey_date', value)}
                                KeyboardButtonProps={{
                                    'aria-label': 'Change Survey Date',
                                }}
                                fullWidth
                            />
                        </MuiPickersUtilsProvider>
                        <Autocomplete
                            id="commodity-autocomplete"
                            onChange={(e, value) => formik.setFieldValue('commodity', value)}
                            options={!loading ? data.commodity.get : []}
                            getOptionLabel={(option) => option.commodity_name }
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formik.errors.commodity} 
                                    label="Choose Commodity" 
                                    margin="dense" 
                                />
                            }/>
                        <NumberFormat 
                            id="commodity_price" 
                            label="Commodity Price" 
                            fullWidth
                            customInput={TextField} 
                            helperText={formik.errors.commodity_price}
                            onValueChange={(value) => formik.setFieldValue('commodity_price', value.value)}
                            thousandSeparator={true} 
                            prefix={'Rp '} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenAddDialog(false)} variant="contained" size="small" color="default">
                            Cancel
                        </Button>
                        <Button type="submit" variant="contained" size="small" color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    const EditSurveyForm = () => {
        const editedValue = data.survey.getForSurveyor.filter((el) => el.id === editSurveyID)[0]
        const editedCommodity = data.commodity.get.filter((el) => el.id === editedValue.commodity_id)[0]

        const EditSurveyMutation = gql`
            mutation editSurvey($id: String!, $survey_date: String!, $commodity_id: String!, $commodity_price: Int!) {
                survey {
                    update(id: $id, survey_date: $survey_date, commodity_id: $commodity_id, commodity_price: $commodity_price)
                }
            }
        `
        const [EditSurveyRequest] = useMutation(EditSurveyMutation)

        const formik = useFormik({
            initialValues: {
                survey_date: editedValue.survey_date,
                commodity: editedCommodity,
                commodity_price: editedValue.commodity_price,
            },
            validationSchema: Yup.object({
                survey_date: Yup.string().required('Survey Date is required.'),
                commodity: Yup.string().required('Commodity is required.'),
                commodity_price: Yup.string().required('Commodity Price is required.'),
            }),
            onSubmit: values => {
                let modifiedValues = {
                    id: editedValue.id,
                    survey_date: values.survey_date,
                    commodity_id: values.commodity.id,
                    commodity_price: parseInt(values.commodity_price),
                }

                EditSurveyRequest({variables: modifiedValues}).then((response) => {
                    setOpenEditDialog(false)
                    enqueueSnackbar('Survey is successfully edited.', {variant: 'success'})
                    refetch()
                }).catch((e) => {
                    setOpenEditDialog(false)
                    console.log(e)
                })
            }
        })

        return (
            <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle id="form-dialog-title">
                        Edit Survey Commodity Data
                        <DialogContentText>
                            To edit survey commodity data, please fill all of form inputs here.
                        </DialogContentText>
                    </DialogTitle>
                    <DialogContent>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                id="survey-date"
                                variant="inline"
                                format="yyyy-MM-dd"
                                margin="dense"
                                label="Survey Date"
                                maxDate={new Date()}
                                helperText={formik.errors.survey_date}
                                value={formik.values.survey_date}
                                onChange={(e, value) => formik.setFieldValue('survey_date', value)}
                                KeyboardButtonProps={{
                                    'aria-label': 'Change Survey Date',
                                }}
                                fullWidth
                            />
                        </MuiPickersUtilsProvider>
                        <Autocomplete
                            id="commodity-autocomplete"
                            onChange={(e, value) => formik.setFieldValue('commodity', value)}
                            options={!loading ? data.commodity.get : []}
                            getOptionLabel={(option) => option.commodity_name }
                            value={formik.values.commodity}
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formik.errors.commodity} 
                                    label="Choose Commodity" 
                                    margin="dense" 
                                />
                            }/>
                        <NumberFormat 
                            id="commodity_price" 
                            label="Commodity Price" 
                            fullWidth
                            customInput={TextField} 
                            helperText={formik.errors.commodity_price}
                            onValueChange={(value) => formik.setFieldValue('commodity_price', value.value)}
                            thousandSeparator={true} 
                            value={formik.values.commodity_price}
                            prefix={'Rp '} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenEditDialog(false)} variant="contained" size="small" color="default">
                            Cancel
                        </Button>
                        <Button type="submit" variant="contained" size="small" color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    const DeleteSurveyMutation = gql`
        mutation deleteSurvey($id: String!) {
            survey {
                delete(id: $id)
            }
        }
    `
    const [DeleteSurveyRequest] = useMutation(DeleteSurveyMutation)

    return (
        <Box component="div">
            <Header 
                title="Survey Commodity"
                breadcrumb={[
                    {display: 'Surveyor', active: true},
                    {display: 'Survey Commodity', active: true}
                ]}>
                    <Box component="div" textAlign="right">
                        <Fab color="primary" size="small" onClick={() => setOpenAddDialog(true)}>
                            <Add /> 
                        </Fab>
                    </Box>
            </Header>
            <Grid container spacing={3}>
                <Grid item xs={12} md={8}>
                    <Box component="div" className={classes.root}>
                        <MaterialTableModified
                            title=""
                            columns={columns}
                            data={editableData}
                            options={{actionsColumnIndex: 6}}
                            actions= {[
                                {
                                    icon: EditOutlined,
                                    tooltip: 'Edit Survey',
                                    onClick: (event, rowData) => {
                                        setEditSurveyID(rowData.id)
                                        setOpenEditDialog(true)
                                    }
                                },
                            ]}
                            editable={{
                                onRowDelete: (oldData) => {
                                    return DeleteSurveyRequest({variables: {id: oldData.id}}).then((response) => {
                                        enqueueSnackbar('Survey data is successfully deleted.', {variant: 'success'})
                                        refetch()
                                    }).catch((e) => {
                                        console.log(e)
                                        refetch()
                                    })
                                }
                            }}
                        />
                    </Box>
                </Grid>
                <Grid item xs={12} md={4}>
                    <Box className={classes.form}>
                        <Typography className="title">Filter</Typography>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                id="filter-date-min"
                                variant="inline"
                                format="yyyy-MM-dd"
                                margin="normal"
                                label="Filter by Date Min"
                                maxDate={new Date()}
                                helperText={formFilter.errors.filter_date_min}
                                value={formFilter.values.filter_date_min}
                                onChange={(e, value) => formFilter.setFieldValue('filter_date_min', value)}
                                KeyboardButtonProps={{ 'aria-label': 'Survey Date Min' }}
                                fullWidth
                            />
                            <KeyboardDatePicker
                                id="filter-date-max"
                                variant="inline"
                                format="yyyy-MM-dd"
                                margin="normal"
                                label="Filter by Date Max"
                                maxDate={new Date()}
                                helperText={formFilter.errors.filter_date_max}
                                value={formFilter.values.filter_date_max}
                                onChange={(e, value) => formFilter.setFieldValue('filter_date_max', value)}
                                KeyboardButtonProps={{ 'aria-label': 'Survey Date max' }}
                                fullWidth
                            />
                        </MuiPickersUtilsProvider>
                        <Autocomplete
                            multiple
                            id="filter-category-autocomplete"
                            onChange={(e, value) => formFilter.setFieldValue('category', value)}
                            options={!loading ? data.category.get : []}
                            getOptionLabel={(option) => option.category_name }
                            size='small'
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formFilter.errors.category} 
                                    label="Filter by Category" 
                                    margin="normal" 
                                />
                            }/>
                        <Autocomplete
                            multiple
                            id="filter-commodity-autocomplete"
                            onChange={(e, value) => formFilter.setFieldValue('commodity', value)}
                            options={!loading ? data.commodity.get : []}
                            getOptionLabel={(option) => option.commodity_name }
                            size='small'
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formFilter.errors.commodity} 
                                    label="Filter by Commodity" 
                                    margin="normal" 
                                />
                            }/>
                    </Box>
                </Grid>
            </Grid>
            <AddSurveyForm />
            {openEditDialog ? <EditSurveyForm /> : null}
        </Box>
    )
}