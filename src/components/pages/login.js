import React from 'react'
import { connect } from "react-redux";
import loginImg from '../../images/Login.svg'
import { makeStyles } from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'
import { gql, useMutation } from '@apollo/client'
import { Redirect } from 'react-router-dom'

const useStyles = makeStyles({
    root: {
        height: '100vh',
        background: 'linear-gradient(30deg, rgba(0,245,255,1) 10%, rgba(136,128,243,1) 57%, rgba(241,37,233,1) 90%)',
        position: 'absolute',
        width: '100%'
    },
    container: {
        width: '100%',
        background: '#fff',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '100px auto 0px auto',
        maxWidth: '350px',
        padding: '30px 10px',
        border: '4px solid lightgrey',
        borderRadius: '4px',
        '& .title': {
            fontWeight: 'bold',
            marginBottom: '10px',
            fontSize: '24px',
            fontFamily: ['Open Sans', 'sans-serif'],
        },
        '& .content': {
            display: 'flex',
            flexDirection: 'column',
            '& .image': {
                width: '21em',
                '& img': {
                    width: '100%',
                    height: '100%',
                }
            }
        }
    },
    form: {
        paddingTop: '2em',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& .form-group': {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex start',
            width: 'fit-content',
            '& label': {
                fontSize: '18px',
            },
            '& input': {
                marginTop: '6px',
                minWidth: '18em',
                height: '37px',
                padding: '0px 10px',
                fontSize: '16px',
                fontFamily: ['Open Sans', 'sans-serif'],
                backgroundColor: '#f3f3f3',
                border: '0px',
                borderRadius: '4px',
                marginBottom: '31px',
                transition: 'all 250ms ease-in-out',
                '&:focus': {
                    outline: 'none',
                    boxShadow: '0px 0px 12px 0.8px #0e81ce96',
                }
            }
        }
    },
    footer: {
        padding: '0px 10px',
        textAlign: 'right',
        marginTop: '1em',
        '& .btn-submit': {
            fontSize: '21px',
            padding: '5px 20px',
            border: '0',
            backgroundColor: '#3498db',
            color: '#fff',
            borderRadius: '3px',
            transition: 'all 250ms ease-in-out',
            cursor: 'pointer',
            '&:hover': {
                backgroundColor: '#2386c8'
            },
            '&:focus': {
                outline: 'none'
            }
        }
    },
})

const mapStateToProps = state => ({
    session: state.session
})

const mapDispatchToProps = dispatch => ({
    authenticate: (payload) => dispatch({ type: 'AUTHENTICATE', payload })
})

function Login ({ containerRef, session, authenticate }) {
    const classes = useStyles()
    const [username, setUsername] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [loginError, setLoginError] = React.useState('')

    const mutation = gql`
        mutation authenticate($username: String!, $password: String!) {
            authenticate(username: $username, password: $password) 
        }
    `
    const [request] = useMutation(mutation)

    const doAuthenticate = async (e) => {
        e.preventDefault()
        await request({variables: {username, password}}).then(async (response) => {
            const {token} = response.data.authenticate
            authenticate(token)
        }).catch((error) => {
            setLoginError(error.message)
        })
    }

    if(session.authenticate) {
        return <Redirect to="/admin/dashboard" />
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <div className={classes.container} ref={containerRef}>
                <div className='title'>Commodity Survey</div>
                <form onSubmit={doAuthenticate}>
                    <div className='content'>
                        <div className='image'>
                            <img src={loginImg} alt="test"/>
                        </div>
                        <div className={classes.form}>
                            <div className='form-group'>
                                <label htmlFor="username">Username</label>
                                <input 
                                    type="text" 
                                    name="username" 
                                    placeholder="Please input username" 
                                    onChange={e => setUsername(e.target.value)} 
                                    value={username}/>
                            </div>
                            <div className='form-group'>
                                <label htmlFor="password">Password</label>
                                <input 
                                    type="password" 
                                    name="password" 
                                    placeholder="Please input password" 
                                    onChange={e => setPassword(e.target.value)} 
                                    value={password}/>
                            </div>
                            {loginError}
                        </div>
                    </div>
                    <div className={classes.footer}>
                        <button type="submit" className='btn-submit'>
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)