import React from 'react'
import Header from '../helper/header'
import { Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import MaterialTable from '../helper/material-table'
import {gql, useQuery, useMutation} from '@apollo/client'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles({
    root: {
        marginTop: '10px',
        marginLeft: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
})

export default function Unit (props) {
    const classes = useStyles()
    const {enqueueSnackbar} = useSnackbar()
    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Unit Name', field: 'unit_name' },
    ])
    
    const getUnitQuery = gql`{unit{ get }}`
    const {loading, data, refetch} = useQuery(getUnitQuery)
    const editableData = !loading ? data.unit.get.map(o => ({ ...o })) : []

    const addUnitMutation = gql`
        mutation addUnit($unit_name: String!) {
            unit {
                create(unit_name: $unit_name)
            }
        }
    `
    const updateUnitMutation = gql`
        mutation updateUnit($id: String!, $unit_name: String!) {
            unit {
                update(id: $id, unit_name: $unit_name)
            }
        }
    `
    const deleteUnitMutation = gql`
        mutation deleteUnit($id: String!) {
            unit {
                delete(id: $id)
            }
        }
    `
    const [addUnitRequest] = useMutation(addUnitMutation)
    const [updateUnitRequest] = useMutation(updateUnitMutation)
    const [deleteUnitRequest] = useMutation(deleteUnitMutation)

    return (
        <Box component="div">
            <Header 
                title="Unit of Commodity"
                breadcrumb={[
                    {display: 'Master Data', active: true},
                    {display: 'Unit of Commodity', active: true}
                ]}/>
            <Box component="div" className={classes.root}>
                <MaterialTable
                    title=""
                    columns={columns}
                    data={editableData}
                    options={{ actionsColumnIndex: 2 }}
                    editable={{
                        onRowAdd: async (newData) => {
                            return addUnitRequest({variables: newData}).then((response) => {
                                enqueueSnackbar('Unit is successfully added.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowUpdate: async (newData, oldData) => {
                            return updateUnitRequest({variables: {...newData, id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Unit is successfully updated.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                        onRowDelete: async (oldData) => {
                            return deleteUnitRequest({variables: {id: oldData.id}}).then((response) => {
                                enqueueSnackbar('Unit is successfully deleted.', {variant: 'success'})
                                refetch()
                            }).catch((err) => {
                                enqueueSnackbar(err.message, {variant: 'warning'})
                                console.log(err)
                            })
                        },
                    }}
                />
            </Box>
        </Box>
    )
}