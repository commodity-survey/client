import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Box, Paper, Tabs, Tab, Chip, Button } from '@material-ui/core'
import Header from '../helper/header'
import { CachedOutlined, DoneOutlined, ThumbDownAltOutlined, CloseOutlined } from '@material-ui/icons'
import { Dialog, DialogActions, DialogContentText, DialogTitle} from '@material-ui/core'

import DateFnsUtils from '@date-io/date-fns'
import MaterialTableModified from '../helper/material-table'
import NumberFormat from 'react-number-format'

import { gql, useQuery, useMutation } from '@apollo/client'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles((theme) => ({
    content: {
        marginTop: '15px',
    },
    form: {
        padding: '8px 10px',
        boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
        '& .title': {
            fontWeight: 'bold',
            fontSize: '1rem'
        },
    },
    table: {
        marginTop: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
}))

export default function SurveyApproval() {
    const classes = useStyles()
    const [TabsActive, setTabsActive] = React.useState('waiting')
    const [approveDialog, setApproveDialog] = React.useState(false)
    const [rejectDialog, setRejectDialog] = React.useState(false)
    const [selectedID, setSelectedID] = React.useState('')

    const dateFns = new DateFnsUtils();
    const {enqueueSnackbar} = useSnackbar()

    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Survey Date', render: (row) => dateFns.format(new Date(row.survey_date), 'dd MMM yyyy') },
        { title: 'Category', field: 'category_name' },
        { title: 'Commodity Name', field: 'commodity_name' },
        { 
            title: 'Commodity Price', 
            render: (row) => <NumberFormat value={row.commodity_price} displayType={'text'} thousandSeparator={true} prefix={'Rp '} /> 
        },
        { 
            title: 'Approval', 
            render: (row) => {
                if(row.approved === 0) {
                    return <Chip
                        icon={<CachedOutlined />}
                        label="Waiting Approval"
                        variant="outlined"
                    />
                } else if (row.approved === 1) {
                    return <Chip
                        icon={<DoneOutlined />}
                        label="Approved"
                        color="primary"
                        variant="outlined"
                    />
                } else {
                    return <Chip
                        icon={<ThumbDownAltOutlined />}
                        label="Rejected"
                        color="secondary"
                        variant="outlined"
                    />
                }
            }
        },
    ])

    const getSurveyQuery = gql`{ approval { getForApproval { approved, rejected, waiting } } }`
    const {loading, data, refetch} = useQuery(getSurveyQuery)
    const waitingApproval = !loading ? data.approval.getForApproval.waiting.map(o => ({ ...o })) : []
    const approved = !loading ? data.approval.getForApproval.approved.map(o => ({ ...o })) : []
    const rejected = !loading ? data.approval.getForApproval.rejected.map(o => ({ ...o })) : []

    const ApproveMutation = gql`mutation approveMutation($id: String!) { survey { approve(id: $id) } }`
    const [ApproveRequest] = useMutation(ApproveMutation)
    const RejectMutation = gql`mutation rejectMutation($id: String!) { survey { reject(id: $id) } }`
    const [RejectRequest] = useMutation(RejectMutation)

    const approveActions = () => {
        ApproveRequest({variables: {id: selectedID}}).then((response) => {
            enqueueSnackbar('Survey data is successfully approved.', {variant: 'success'})
            setApproveDialog(false)
            refetch()
        }).catch((e) => {
            setApproveDialog(false)
            console.log(e)
            refetch()
        })
    }

    const rejectActions = () => {
        RejectRequest({variables: {id: selectedID}}).then((response) => {
            enqueueSnackbar('Survey data is successfully rejected.', {variant: 'success'})
            setRejectDialog(false)
            refetch()
        }).catch((e) => {
            setRejectDialog(false)
            console.log(e)
            refetch()
        })
    }

    return (
        <div className={classes.root}>
            <Header 
                title="Survey Approval" 
                subtitle="Decide to Approve or Reject Survey Data" />
            <Paper classes={{root: classes.content}}>
                <Tabs value={TabsActive} onChange={(e, val) => setTabsActive(val)} aria-label="wrapped label tabs example">
                    <Tab id="tab-waiting" icon={<CachedOutlined />} aria-controls="tabpanel-waiting" label="Waiting Approval" value="waiting" />
                    <Tab id="tab-approved" icon={<DoneOutlined />} aria-controls="tabpanel-approved" value="approved" label="Approved" />
                    <Tab id="tab-rejected" icon={<ThumbDownAltOutlined />} aria-controls="tabpanel-rejected" value="rejected" label="Rejected" />
                </Tabs>
                <Box role="tabpanel" className={classes.table} hidden={TabsActive !== "waiting"} id={`wrapped-tabpanel-waiting`} aria-labelledby={`wrapped-tab-waiting`}>
                    <MaterialTableModified title="Waiting Approval Price" columns={columns} data={waitingApproval} options={{actionsColumnIndex: 6}}
                        actions= {[
                            {
                                icon: DoneOutlined,
                                tooltip: 'Approve',
                                onClick: (event, rowData) => {
                                    setSelectedID(rowData.id)
                                    setApproveDialog(true)
                                }
                            },
                            {
                                icon: CloseOutlined,
                                tooltip: 'Reject',
                                onClick: (event, rowData) => {
                                    setSelectedID(rowData.id)
                                    setRejectDialog(true)
                                }
                            },
                        ]}
                    />
                </Box>
                <Box role="tabpanel" className={classes.table} hidden={TabsActive !== "approved"} id={`wrapped-tabpanel-approved`} aria-labelledby={`wrapped-tab-approved`}>
                    <MaterialTableModified title="Approved Price" columns={columns} data={approved} options={{actionsColumnIndex: 6}} />
                </Box>
                <Box role="tabpanel" className={classes.table} hidden={TabsActive !== "rejected"} id={`wrapped-tabpanel-rejected`} aria-labelledby={`wrapped-tab-rejected`}>
                    <MaterialTableModified title="Rejected Price" columns={columns} data={rejected} options={{actionsColumnIndex: 6}} />
                </Box>
            </Paper>
            {
                approveDialog ? (
                    <Dialog open={approveDialog} onClose={() => setApproveDialog(false)} aria-labelledby="approve-dialog">
                        <DialogTitle id="approve-dialog">
                            Approve Survey Data
                            <DialogContentText>
                                Are you sure you want to approve this survey data ?
                            </DialogContentText>
                        </DialogTitle>
                        <DialogActions>
                            <Button onClick={() => setApproveDialog(false)} variant="contained" size="small" color="default">
                                Cancel
                            </Button>
                            <Button onClick={approveActions} variant="contained" size="small" color="primary">
                                Approve
                            </Button>
                        </DialogActions>
                    </Dialog>
                ) : null
            }
            {
                rejectDialog ? (
                    <Dialog open={rejectDialog} onClose={() => setRejectDialog(false)} aria-labelledby="reject-dialog">
                        <DialogTitle id="reject-dialog">
                            Reject Survey Data
                            <DialogContentText>
                                Are you sure you want to reject this survey data ?
                            </DialogContentText>
                        </DialogTitle>
                        <DialogActions>
                            <Button onClick={() => setRejectDialog(false)} variant="contained" size="small" color="default">
                                Cancel
                            </Button>
                            <Button onClick={rejectActions} variant="contained" size="small" color="secondary">
                                Reject
                            </Button>
                        </DialogActions>
                    </Dialog>
                ) : null
            }
        </div>
    )
}
