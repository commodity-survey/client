import React from 'react'
import Header from '../helper/header'
import { Box, Button, TextField, Fab } from '@material-ui/core'
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import { VisibilityOutlined } from '@material-ui/icons'
import { Autocomplete } from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'
import { gql, useQuery, useMutation } from '@apollo/client'
import { useSnackbar } from 'notistack'
import { Add } from '@material-ui/icons'
import { useFormik } from 'formik'
import * as Yup from 'yup'

import MaterialTableModified from '../helper/material-table'

const useStyles = makeStyles({
    root: {
        marginTop: '10px',
        "& .MuiPaper-root": {
            padding: '0px 15px'
        }
    }
})

export default function Account (props) {
    const classes = useStyles()
    const [openAddDialog, setOpenAddDialog] = React.useState(false)
    const [columns] = React.useState([
        { title: '#', render: (row) => row.tableData.id + 1, width: 50 },
        { title: 'Email', field: 'username' },
        { title: 'Full Name', field: 'name' },
        { title: 'Address', field: 'address' },
        { title: 'Roles', render: (row) => { return row.roles.map(role => role.roledesc).join(', ') } },
    ])
    
    const getAccountQuery = gql`{account{ get, roles }}`
    const {loading, data, refetch} = useQuery(getAccountQuery)
    const editableData = !loading ? data.account.get.map(o => ({ ...o })) : []

    const AddAccountForm = () => {
        const {enqueueSnackbar} = useSnackbar()
    
        const AddAccountMutation = gql`
            mutation addAccount($username: String!, $name: String!, $address: String!, $password: String!, $repassword: String!, $roles: [String]!) {
                account {
                    create(username: $username, name: $name, address: $address, password: $password, repassword: $repassword, roles: $roles)
                }
            }
        `
        const [AddAccountRequest] = useMutation(AddAccountMutation)

        const formik = useFormik({
            initialValues: {
                username: '',
                name: '',
                address: '',
                password: '',
                repassword: '',
                roles: [],
            },
            validationSchema: Yup.object({
                username: Yup.string().email('Email does not valid').required('Email is required.'),
                name: Yup.string().required('Name is required.'),
                address: Yup.string().required('Address is required.'),
                password: Yup.string().required('Password is required.'),
                repassword: Yup.string().required('Repassword is required.').oneOf([Yup.ref('password')], 'Password does not match'),
                roles: Yup.array().required('Roles is required.'),
            }),
            onSubmit: values => {
                values.roles = values.roles.map(val => val.rolename)

                AddAccountRequest({variables: values}).then((response) => {
                    setOpenAddDialog(false)
                    enqueueSnackbar('Packages is successfully added.', {variant: 'success'})
                    refetch()
                }).catch((e) => {
                    setOpenAddDialog(false)
                    console.log(e)
                })
            }
        })

        return (
            <Dialog open={openAddDialog} onClose={() => setOpenAddDialog(false)} aria-labelledby="form-dialog-title">
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle id="form-dialog-title">
                        Add Account Data
                        <DialogContentText>
                            To add account data, please fill all of form input here.
                        </DialogContentText>
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            helperText={formik.errors.username}
                            margin="dense"
                            onChange={formik.handleChange}
                            id="username" label="Email" type="text" fullWidth
                            value={formik.values.username}/>
                        <TextField
                            helperText={formik.errors.name}
                            margin="dense"
                            onChange={formik.handleChange} 
                            id="name" label="Full Name" type="text" fullWidth
                            value={formik.values.name}/>
                        <TextField
                            helperText={formik.errors.address}
                            margin="dense"
                            onChange={formik.handleChange}
                            id="address" label="Address" type="text" fullWidth
                            value={formik.values.address}/>
                        <TextField
                            helperText={formik.errors.password}
                            margin="dense"
                            onChange={formik.handleChange}
                            id="password" label="Password" type="password" fullWidth
                            value={formik.values.password}/>
                        <TextField
                            helperText={formik.errors.repassword}
                            margin="dense"
                            onChange={formik.handleChange}
                            id="repassword" label="Repassword" type="password" fullWidth
                            value={formik.values.repassword}/>
                        <Autocomplete
                            multiple
                            id="roles-autocomplete"
                            onChange={(e, value) => formik.setFieldValue('roles', value)}
                            value={formik.values.roles}
                            options={!loading ? data.account.roles : []}
                            getOptionLabel={(option) => option.roledesc }
                            renderInput={(params) => <TextField 
                                    {...params}
                                    helperText={formik.errors.roles} 
                                    label="Roles" 
                                    margin="dense" 
                                />
                            }/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setOpenAddDialog(false)} variant="contained" color="default">
                            Cancel
                        </Button>
                        <Button type="submit" variant="contained" color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        )
    }

    return (
        <Box component="div">
            <Header 
                title="Account Management"
                breadcrumb={[
                    {display: 'Super Admin', active: true},
                    {display: 'Account Management', active: true}
                ]}>
                    <Box component="div" textAlign="right">
                        <Fab color="primary" size="small" onClick={() => setOpenAddDialog(true)}>
                            <Add /> 
                        </Fab>
                    </Box>
            </Header>
            <Box component="div" className={classes.root}>
                <MaterialTableModified
                    title=""
                    columns={columns}
                    data={editableData}
                    options={{actionsColumnIndex: 5}}
                    actions= {[
                        {
                            icon: VisibilityOutlined,
                            tooltip: 'View Detail Account',
                            onClick: (event, rowData) => {
                                props.history.push(`/admin/account/${rowData.username}`)
                            }
                        },
                    ]}
                    editable={{
                        onRowDelete: () => {

                        }
                    }}
                />
            </Box>
            <AddAccountForm />
        </Box>
    )
}