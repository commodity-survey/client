import React from 'react'
import Header from '../helper/header'
import { Box } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    'content': {
        margin: '10px',
        '& .coming-soon': {
            fontWeight: 'bold',
            fontSize: '1.5rem'
        }
    }
})

export default (props) => {
    const classes = useStyles()
    return (
        <Box component="div">
            <Header title="Dashboard" subtitle="General Statistic Overview" />
            <Box className={classes.content}>
                <Alert severity="info">Coming Soon</Alert>
            </Box>
        </Box>
    )
}