import React from 'react'
import { connect } from "react-redux";
import { Drawer, Grid, Box, AppBar, Toolbar, Typography, Button, Menu, MenuItem, List, ListItem, ListItemText, ListItemIcon, Divider, Hidden } from '@material-ui/core'
import { withRouter } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { Settings, Menu as MenuIcon } from './admin-icons'
import * as Icons from './admin-icons'
import defaultDP from '../../images/avatar_male.png'
import env from '../../config/env'

const useStyles = makeStyles(theme => ({
    /** Header */
    header: {
        zIndex: '1250',
    },
    toolbar: {
        minHeight: '44px'
    },
    brand: {
        display: 'flex',
        flex: 1,
    },
    brandText: {
        paddingTop: '5px',
        fontSize: '1.2rem',
        width: '210px',
    },
    menuIcon: {
        color: 'white',
    },
    rightIcon: {
        color: 'white',
    },

    /** Sidebar */
    display: {
        textAlign: 'center',
        marginTop: '30px',
        padding: '20px 10px 10px 10px',
    },
    displayRole: {
        fontSize: '0.8rem',
        fontWeight: 'bold'
    },
    displayPicture: {
        width: '90px',
        height: '90px',
        padding: '10px',
        borderRadius: '100px'
    },
    sidebar: {
        width: '240px',
        transform: 'none',
    },
    sidebarhide: {
        transition: 'transform 200ms cubic-bezier(2, 0, 0, 0) 200ms',
        transform: 'translateX(-240px)',
    },
    labelMenu: {
        fontSize: '12px',
        padding: '3px 10px',
        color: 'darkgray',
    },
    list: {
        paddingLeft: '4px'
    },
    listItem: {
        padding: '4px 16px',
        '&.active': {
            borderLeft: '3px solid blue',
            background: 'lightblue'
        }
    },
    listItemIcon: {
        minWidth: '40px',
    },
    listItemText: {
        fontSize: '0.8rem',
        paddingTop: '4px',
        color: '#000'
    },

    /** Content */
    main: {
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        marginTop: '35px',
        '& .MuiDrawer-paper': {
            width: '240px'
        }
    },
    content: {
        width: '100%',
        padding: '5px',
    },
    footer: {
        bottom: '0',
        left: 'auto',
        right: '0',
        position: 'fixed',
        zIndex: '1250',
        backgroundColor: '#EEEEEE',
        width: '100%',
        height: '35px',
        paddingTop: '10px',
        '& .footer-text': {
            padding: '0px 20px',
            fontWeight: 'bold'
        }
    }
}))

function Admin ({ history, children, activateMenu, session, menu, logout }) {
    const classes = useStyles()

    const [sidebar, setSidebar] = React.useState(true)
    const [anchor, setAnchor] = React.useState(null)

    const Display = () => {
        return (
            <Box className={classes.display}>
                <img 
                    className={classes.displayPicture} 
                    src={session.userinfo.hasPhoto ? `http://${env.hostname}/${env.api}/file/display-picture?token=${session.token}`: defaultDP} 
                    alt="Default Profile" />
                <Typography>{session.userinfo.name}</Typography>
                <Typography className={classes.displayRole}>Administrator</Typography>
            </Box>
        )
    }

    const ListMenu = () => {
        return (
            <List className={classes.list}>
                {session.userinfo.menu.map((list) => {
                    return (
                        <span key={list.label}>
                            <Typography className={classes.labelMenu}>{list.label}</Typography>
                            {list.menu.map((item) => {
                                const ListIcon = Icons[item.icons]
                                return (
                                    <ListItem 
                                        className={item.url === menu ? 'active':''} 
                                        classes={{root: classes.listItem}} 
                                        key={item.url}
                                        onClick={() => {
                                            activateMenu(item.url)
                                            history.push(item.url)
                                        }}
                                        button>
                                        <ListItemIcon classes={{root: classes.listItemIcon}}><ListIcon /></ListItemIcon>
                                        <ListItemText classes={{root: classes.listItemText}} primary={item.title} />
                                    </ListItem>
                                )
                            })}
                        </span>
                    )
                })}
            </List>
        )
    }

    const Footer = () => {
        return (
            <Box className={classes.footer}>
                <Typography className="footer-text">
                    Copyright &copy; 2020 Moch. Annafia O. All right reserved.
                </Typography>
            </Box>
        )
    }

    const Logout = () => { setAnchor(null); logout(); }

    return (
        <Grid container direction="row">
            <AppBar classes={{root: classes.header}}>
                <Toolbar classes={{root: classes.toolbar}}>
                    <Box className={classes.brand}>
                        <Typography className={classes.brandText}>
                            {env.brand}
                        </Typography>
                        <Button onClick={() => { setSidebar(!sidebar) }}>
                            <MenuIcon className={classes.menuIcon} />
                        </Button>
                    </Box>
                    <Button aria-controls="simple-menu" aria-haspopup="true" onClick={(e) => setAnchor(e.currentTarget)}>
                        <Settings className={classes.rightIcon} />
                    </Button>
                    <Menu 
                        id="simple-menu"
                        keepMounted
                        anchorEl={anchor}
                        open={Boolean(anchor)}
                        onClose={() => setAnchor(null)}
                        classes={{paper: classes.rightMenu}}>
                        <MenuItem onClick={() => setAnchor(null)}>My Account</MenuItem>
                        <MenuItem onClick={() => Logout()}>Logout</MenuItem>
                    </Menu>
                </Toolbar>
            </AppBar>
            <Box className={classes.main}>
                <Hidden xsDown>
                    <Drawer
                        classes={{ root: sidebar ? classes.sidebar : classes.sidebarhide }}
                        variant="persistent"
                        open={sidebar} >
                        <Display /> 
                        <Divider /> 
                        <ListMenu />
                    </Drawer>
                </Hidden>
                <Hidden smUp>
                    <Drawer
                        classes={{ 
                            root: !sidebar ? classes.sidebar : classes.sidebarhide,
                            paper: classes.paper
                        }}
                        variant='temporary'
                        open={!sidebar} >
                            <AppBar>
                                <Toolbar classes={{root: classes.toolbar}}>
                                    <Box className={classes.brand}>
                                        <Typography className={classes.brandText}>
                                            {env.brand}
                                        </Typography>
                                        <Button onClick={() => { setSidebar(!sidebar) }}>
                                            <MenuIcon className={classes.menuIcon} />
                                        </Button>
                                    </Box>
                                    <Button onClick={() => { setSidebar(!sidebar) }}>
                                        <Settings className={classes.rightIcon} />
                                    </Button>
                                </Toolbar>
                            </AppBar>
                            <Display /> 
                            <Divider /> 
                            <ListMenu />
                            <Footer />
                    </Drawer>
                </Hidden>
                <div className={classes.content}>
                    {children.map((child, key) => {
                        const RenderComponent = withRouter(child.props.component)
                        const ChildComponent = React.cloneElement(child, { 
                            ...child.props,
                            key: key,
                            component: null,
                            render: () => {
                                const activeMenuTemp = child.props.activeMenu ? child.props.activeMenu : child.props.path
                                activateMenu(activeMenuTemp)
                                return <RenderComponent />
                            }
                        })
                        
                        return ChildComponent
                    })}
                </div>
            </Box>
            <Footer />
        </Grid>
    )
}

const mapStateToProps = state => ({
    session: state.session,
    menu: state.menu
})

const mapDispatchToProps = dispatch => ({
    activateMenu: (payload) => dispatch({type: 'ACTIVATE_MENU', payload}),
    logout: () => dispatch({type: 'LOGOUT'}),
})

export default connect(mapStateToProps, mapDispatchToProps)(Admin)